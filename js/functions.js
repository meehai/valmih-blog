function makeElementFollowScroll(target) {
	var elementStartPosition = $(target).offset().top;
	var elementOriginalWidth = $(target).width();
	var elementOriginalHeight = $(target).height();

	function f() {
		var scrollTop = $(window).scrollTop();
		if(elementStartPosition <= scrollTop) {
			$(target).css({ "position": "fixed", "top": "0",
				"width": elementOriginalWidth, "height" : elementOriginalHeight});
		}
		else {
			$(target).css({ "position": "" });
		}
	}

	f();
	$(window).on("scroll", function (event) {
		f();
	});
}

function updateBlogDivs() {
	if ($(".blogPostAddLeft").length) {
		$("body").css({"margin" : "0"});
		$(".contentLeft, .contentRight").css({"display" : "none"});
	}
}

function updateLivePreviewAdd() {
	function f() {
		if(!$(".blogTextArea").length) {
			return;
		}

		textArea = $(".blogTextArea").val();
		title = $(".blogTitle").val();
		tags = $(".blogTags").val();
		author = $("#blogAuthor").html();
		date = $("#blogDate").html();

		content =
			"<div class=\"content\" style=\"width:100%; position:relative;left: 0; marign: 0; padding: 0;\">" +
				"<div class=\"blogContentPage\">" +
					"<div class=\"blogPostTitle\">" + title + "</div>" +
					"<div class=\"blogPostAuthor\"> Posted by: " + author + "</div>" +
					"<div class=\"blogPostDate\"> Date: " + date + "</div>" +
					"<div class=\"blogPostTags\">" + "Tags: " + tags + "</div>" +
					textArea +
				"</div>" +
			"</div";

		$(".blogPostAddRight").html(content);
		updateNewLines();
	}

	f();
	/* This is here so createSectionTitle isn't called twice. */
	$(".blogPostAddLeft").on("keyup", function() {
		f();
	});
}

/* The text (and only it) shall have the \n replaced by <br>. Somehow only this works (TODO). Other methods seem to
 *  fail for live editing. */
function updateNewLines() {
	function updateNewLinesFn(text) {
		text = text.replace("\n\n\n\n\n", "\n<br/><br/><br/><br/>");
		text = text.replace("\n\n\n\n", "\n<br/><br/><br/>");
		text = text.replace("\n\n\n", "\n<br/><br/>");
		text = text.replace("\n\n", "\n<br/>");
		return text
	}

	$("section").each(function () {
		text = $(this).html();
		text = updateNewLinesFn(text);
		$(this).html(text);
	});
}

function updateLivePreviewSave() {
	if (!$(".liveEditSaveButton").length) {
		return;
	}

	function doAjaxSave() {
		$.ajax({
			type: "POST",
			url: window.location.href,
			data: { saveButton: "", blogTitle: $(".blogTitle").val(), blogTags: $(".blogTags").val(),
				blogFileContent: $(".blogTextArea").val() },
			success: function (result) {
				$("#liveEditSavedMessage").html("Saved!").stop(true, true).show().fadeOut(1000);
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				alert("Error saving.\nStatus: " + textStatus + "\nError: " + errorThrown);
			}
		});
	}

	$(".liveEditSaveButton").on("click", function(event) {
		event.preventDefault();
		doAjaxSave();
	});

	$(window).bind("keydown", function (event) {
		if (event.ctrlKey || event.metaKey) {
			switch (String.fromCharCode(event.which).toLowerCase()) {
				case "s":
					event.preventDefault();
					doAjaxSave();
					break;
			}
		}
	});
}


function createSectionsListLeft() {
	function recursiveFindSections(currentElement = "body", lookedElement = "section", prefix = "") {
		Str = "";
		var i = 0;
		$(currentElement).find(lookedElement).each(function () {
			if(i == 0) {
				Str += "<ul>";
			}

			i += 1;
			ix = prefix + i + ".";
			thisName = $(this).attr("name");
			/* subsubsection_name */
			tag = lookedElement + "_" + thisName;
			$(this).attr("id", tag);
			Str += "<li>" + "<a href=\"#" + tag + "\">" +
				ix + thisName +
				recursiveFindSections($(this), "sub" + lookedElement, ix) +
				"</a>" + "</li>";
		});

		if(i > 0) {
			Str += "</ul>";
		}
		return Str;
	}


	if (!$(".blogSectionsLeft").length) {
		return;
	}

	items = recursiveFindSections();
	if(items == "") {
		return;
	}
	Str = "<div class=\"menuContent\">Content"
			+ items
		+ "</div>";
	$(".menuBox").append(Str);
	$(".blogSectionsLeft").addClass("sideBarBox").appendTo(".contentLeft");
}

function updateSectionTitles() {
	var i = 1;
	$("body").find("section").each(function () {
		thisName = $(this).attr("name");
		Str = "<h2>" + i + ". " + thisName + "</h2>"
		$(this).prepend(Str);
		i += 1;
	});
}

function runWithJQuery(jQueryCode){
	if(window.jQuery) {
		jQueryCode();
	}
	else {
		var script = document.createElement("script");
		document.head.appendChild(script);
		script.type = "text/javascript";
		script.src = "https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js";
		script.onload = jQueryCode;
	}
}

runWithJQuery(function jQueryCode(){
	updateLivePreviewAdd();
	updateLivePreviewSave();
	createSectionsListLeft();
	updateSectionTitles();
});