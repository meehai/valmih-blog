<?php

class blog extends Module {

	function __construct($connection, $modulePath) {
		parent::__construct($connection, $modulePath);
		includeJS(Constants::$modulesWebPath . "/blog/js/functions.js");
		includeJS("https://i.upmath.me/latex.js");
	}

	function addBlogPost($title, $tags, $content, $userName) {
		if(strlen($title) == 0) {
			echo "Title or filename are invalid.";
			return false;
		}

		$content = prepareStringForDB($content);
		$title = prepareStringForDB($title);
		$tags = prepareStringForDB(trim($tags));
		$userId = $this->connection->getUserID($userName);
		$now = time();

		$this->connection->insertInto("blog", ["userId", "title", "tags", "content", "date", "visible"],
			[$userId, $title, $tags, $content, $now, 0]);
		$id = $this->connection->getLastInsertedId();
		return $id;
	}

	function updateBlogPost($id, $title, $tags, $content) { 
		if(strlen($title) == 0) {
			echo "Title or filename are invalid.";
			return false;
		}

		$content = prepareStringForDB($content);
		$title = prepareStringForDB($title);
		$tags = prepareStringForDB(trim($tags));

		$this->connection->updateTable("blog", ["title", "tags", "content"], [$title, $tags, $content], "id='$id'");
	}

	function getBlogPostFromDB($id) {
		$items = $this->connection->getInfo("userId,title,tags,content,date,visible", "blog", "id='$id'", "first_row");
		if(count($items) == 0) {
            return NULL;
        }

		$userName = $this->connection->getInfo("username", "users", "id=$items[userId]", "first_row,first_value");
		$items["author"] = $userName;
		$items["formattedDate"] = date("Y-m-d H:i:s", $items["date"]);
		return $items;
	}

	function getBlogPosts() {
		$query = "SELECT * from blog ORDER BY date asc";
		$blogPosts = $this->connection->query($query)->fetchArray("assoc");

		$formattedBlogPosts = [];
		foreach ($blogPosts as $blogPost) {
			$userName = $this->connection->getInfo("username", "users", "id=$blogPost[userId]",
				"first_row,first_value");
			$blogPostDate = date("Y-m-d H:i:s", $blogPost["date"]);
			$formattedBlogPosts[] = ["author" => $userName, "date" => $blogPost["date"],
				"formattedDate" => $blogPostDate, "title" => $blogPost["title"], "userId" => $blogPost["userId"],
				"id" => $blogPost["id"], "tags" => $blogPost["tags"], "visible" => $blogPost["visible"],
				"content" => $blogPost["content"]];
		}
		return $formattedBlogPosts;
	}

	function loadBlogPost($id) {
        $items = $this->getBlogPostFromDB($id);
        if(!$items) {
            echo "Wrong blog post id!";
			redirect("blog", 3);
			exit;
		}
		return $items;
	}

	function install() {
		$this->connection->createTable("blog", ["id", "userId", "title", "tags", "content", "date", "visible"],
			["integer", "varchar(50)", "varchar(50)", "varchar(50)", "blob", "integer", "integer"]);

		/* TODO: Add dummy blogpost */
	}

	function toggleVisibility($id) {
		$blogPost = $this->getBlogPostFromDB($id);
		$sessionUserId = Page::getInstance()->getModule("login")->getUserId();
		assert($blogPost["userId"] == $sessionUserId);

		$currentVisibility = $blogPost["visible"];
		$newVisibility = ($currentVisibility + 1) % 2;

		$this->connection->updateTable("blog", "visible", $newVisibility, "id=$id");
	}
}

?>